# k-mer distances



## Getting started

Needed python packages:

    - SeqIO from Bio
    - os
    - hashlib
    - Phylo from Bio
    - Seq from Bio.Seq
    - argparse

Running the script:
```
git clone https://gitlab.com/johannahelene/k-mer-distances.git
cd repository_dir
conda env create kmers.yml
conda actiavte kmer-distances
python k-mer_distances -i path/to/input/fastas -o path/to/output/directory -k kmersize
```

Additinal Parameters:
```
-s sketchsize
```
