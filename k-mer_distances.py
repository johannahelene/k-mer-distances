from Bio import SeqIO
import os
import hashlib
import numpy as np
from Bio import Phylo
from Bio.Phylo.TreeConstruction import DistanceTreeConstructor
from Bio.Phylo.TreeConstruction import DistanceMatrix
from Bio.Seq import Seq
import argparse

def get_sequence(file, kmer_length, input_directory):
    # step 1:
    # Write code to read in the DNA sequence from the FASTA files
    kmer_dict_seq = {}
    input = os.path.join(input_directory, file)
    for seqs in SeqIO.parse(input, 'fasta'):
        for i in range(0, len(seqs.seq)):
            # count the occurences of 14-mers in sequence 1, storing these in a dictionary
            kmer = seqs.seq[i: i+kmer_length]
            if len(kmer) == kmer_length:
                kmer_dict_seq[kmer] = kmer_dict_seq.get(kmer, 0) + 1
    kmer_list_seq = [key for key in kmer_dict_seq.keys()]
    return kmer_list_seq

def calculate_jaccard(seq1, seq2):
    # step 2:
    # write code which calculates the Jaccard distance between two of the input samples
    # list comprehension way to slow for full set
    # intersection = len([value for value in seq1 if value in seq2])
    inters = len(set.intersection(set(seq1), set(seq2)))
    union = (len(seq1) + len(seq2)) - inters
    jaccard_index = abs(inters)/abs(union)
    jaccard_distance = 1-jaccard_index
    # print('The Jaccard Index =', jaccard_index)
    # print('The Jaccard Distance =', jaccard_distance)
    return([jaccard_index, jaccard_distance])

def create_sketch(kmer_list_seq, s, file_name, out):
    # step 4:
    # Make a sketch of each of the input sequences
    #   1. Converting all 14-mers from the genome into integers using a hash function.
    # for km in kmer_list_seq:
    #     print(km, type(km))
    hash_seq = [int(hashlib.sha256(bytes(str(kmer), 'utf-8')).hexdigest(), base = 32) for kmer in kmer_list_seq]
    #   2. Sorting these hashes
    hash_seq_sorted = sorted(hash_seq)
    #   3. Taking the smallest s hashes, where s=1000 is the ‘sketch size’. This can be made larger or smaller, but for this purpose we’ll use a fixed value of 1000.
    if s == 0:
        sketch_seq = hash_seq_sorted
        name = 'full'
    elif int(s) > 0:
        sketch_seq = hash_seq_sorted[0:int(s)]
        name = str(s)
    # save sketches as textfile or other representation
    output_file = out + '/sketch_' + name + '_' + file_name + '.txt'
    with open(output_file, 'w') as file_handler:
        for hash in sketch_seq:
            file_handler.write("{}\n".format(hash))
    return(sketch_seq)

def MinHash(file, kmer_length, s, testhash, fulljaccard, input_directory, output_directory):
    file_name = os.path.basename(file)
    file_name = os.path.splitext(file_name)[0]
    # step 1
    kmer_list_seq = get_sequence(file, kmer_length, input_directory)
    # if fulljaccard == True:
    #     # step 2:
    #     print('Jaccard:')
    #     calculate_jaccard(kmer_list_seq1, kmer_list_seq2)

    if testhash == 'True' and file == input[0]:
        # step 3:
        # Calculate the hash of some example 14-mers, and confirm that the same 14-mer input maps to the same integer output.
        example_kmers = ['TTGAAAGAAAAACA', 'TGAAAGAAAAACAA', 'GAAAGAAAAACAAT', 'AAAGAAAAACAATT', 'AAGAAAAACAATTT', 'AGAAAAACAATTTT', 'GAAAAACAATTTTG']
        # [(Seq('TTGAAAGAAAAACA'), Seq('TGAAAGAAAAACAA'), Seq('GAAAGAAAAACAAT'), Seq('AAAGAAAAACAATT'), Seq('AAGAAAAACAATTT'), Seq('AGAAAAACAATTTT'), Seq('GAAAAACAATTTTG'))]
        # for blub in example_kmers:
        #     print(blub, type(blub))
        #     print(bytes(blub, 'utf-8'))
        hash1 = [int(hashlib.sha256(bytes(i, 'utf-8')).hexdigest(), base = 32) for i in example_kmers]
        hash2 = [int(hashlib.sha256(bytes(i, 'utf-8')).hexdigest(), base = 32) for i in example_kmers]
        print('Hash 1 is eqaul to Hash 2:', hash1 == hash2)

    # step 4:
    print(file_name)
    sketch_seq = create_sketch(kmer_list_seq, s, file_name, output_directory)

    return sketch_seq

def isFasta(file):
    # to get rid of hidden files
    file_type = ['.fasta', '.fa']
    if os.path.basename(file)[0] != '.' and os.path.splitext(file)[1] in file_type:
        return True

def load_sketches(output_directory):
    input = [sketch for sketch in os.listdir(output_directory) if os.path.isfile(os.path.join(output_directory, sketch))]
    dm = []
    for i in range(0, len(input)):
        row = []
        for j in range(0, len(input)):
            if j <= i:
                # actually access file1 and 2 content
                with open(os.path.join(output_directory, input[i])) as f1:
                    list1 = [line.rstrip() for line in f1]
                with open(os.path.join(output_directory, input[j])) as f2:
                    list2 = [line.rstrip() for line in f2]
                dist = calculate_jaccard(list1, list2)
                row.append(dist[1])
        dm.append(row)
    return dm

def NJtree(distance_matrix, input):
    constructor = DistanceTreeConstructor()
    dm =  DistanceMatrix(input, distance_matrix)
    print(dm)
    tree = constructor.nj(dm)
    return tree

if __name__ == '__main__':

    p = argparse.ArgumentParser("Calculating k-mer distances and MinHash:", usage = '%(prog)s -i fasta/input/path -k int -o output/dir/path [-s sketchsize] [-th testhash]', add_help = False)

    # parameters:
    required = p.add_argument_group('required arguments')
    required.add_argument('-i','--input', action='store', help = 'input directory of fasta files', required = True, metavar='')
    required.add_argument('-k','--kmerlength', action='store',  help = 'length of k-mers', required = True, metavar='')
    required.add_argument('-o', '--output', action='store', required = True, help = 'destination path for sketch files', metavar='')

    optional = p.add_argument_group('optional arguments')
    optional.add_argument('-s', '--sketchsize', action='store', help = 'define the sketch size; when not specified the full set will be used', default = 0, metavar='')
    optional.add_argument('-th','--testhash', action='store', default = False, help = 'print the test hash', metavar='')
    optional.add_argument('-fj','--fulljaccard', action='store', default = False, help = 'print the test hash', metavar='')

    args = p.parse_args()

    # create list with input fasta file to
    input = [fasta for fasta in os.listdir(args.input) if os.path.isfile(os.path.join(args.input, fasta)) and isFasta(os.path.join(args.input, fasta))]

    # steps 1-4 in MinHash
    for file in input:
        MinHash(file, int(args.kmerlength), args.sketchsize, args.testhash, args.fulljaccard, args.input, args.output)

    # step 5:
    # For each pair of inputs, load their sketches, and calculate their Jaccard distance using the sizes of the intersection and the union of the saved hash values
    distance_matrix = load_sketches(args.output)

    # Question2: Use your distances to make a neighbour-joining tree of the four isolates.
    input = [sketch for sketch in os.listdir(args.output) if os.path.isfile(os.path.join(args.output, sketch))]
    tree = NJtree(distance_matrix, input)
    Phylo.draw(tree)

    # Question 1:
    # How do the MinHash distances compare to the full distances? What if you change the sketch size?
    # Question 2:
    # Use your distances to make a neighbour-joining tree of the four isolates. How are they related? Does this change with the two different distances you have computed?
